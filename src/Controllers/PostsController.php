<?php

namespace Dot\Posts\Controllers;

use Action;
use Dot\Platform\Controller;
use Illuminate\Support\Facades\Auth;
use Dot\Posts\Models\Post;
use Dot\Posts\Models\PostMeta;

/**
 * Class PostsController
 * @package Dot\Posts\Controllers
 */
class PostsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Show all posts
     * @return mixed
     */
    function index()
    {

        if (request()->isMethod("post")) {
            if (request()->filled("action")) {
                switch (request()->get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }

        $this->data["sort"] = (request()->filled("sort")) ? request()->get("sort") : "created_at";
        $this->data["order"] = (request()->filled("order")) ? request()->get("order") : "DESC";
        $this->data['per_page'] = (request()->filled("per_page")) ? request()->get("per_page") : NULL;

        $query = Post::with('image', 'user', 'tags')->orderBy($this->data["sort"], $this->data["order"]);

        if (request()->filled("tag_id")) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", request()->get("tag_id"));
            });
        }

        if (request()->filled("category_id") and request()->get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", request()->get("category_id"));
            });
        }

        if (request()->filled("format")) {
            $query->where("format", request()->get("format"));
        }

        if (request()->filled("from")) {
            $query->where("created_at", ">=", request()->get("from"));
        }

        if (request()->filled("to")) {
            $query->where("created_at", "<=", request()->get("to"));
        }

        if (request()->filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", request()->get("user_id"));
            });
        }

        if (request()->filled("status")) {
            $query->where("status", request()->get("status"));
        }

        if (request()->filled("q")) {
            $query->search(urldecode(request()->get("q")));
        }
        $this->data["posts"] = $query->paginate($this->data['per_page']);

        return view()->make("posts::show", $this->data);
    }

    /**
     * Delete post by id
     * @return mixed
     */
    public function delete()
    {
        $ids = request()->get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $post = Post::findOrFail($ID);

            // Fire deleting action

            Action::fire("post.deleting", $post);

            $post->tags()->detach();
            $post->categories()->detach();

            $post->delete();

            // Fire deleted action

            Action::fire("post.deleted", $post);
        }

        return redirect()->back()->with("message", trans("posts::posts.events.deleted"));
    }

    /**
     * Activating / Deactivating post by id
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        $ids = request()->get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $post = Post::findOrFail($id);

            // Fire saving action
            Action::fire("post.saving", $post);

            $post->status = $status;
            $post->save();

            // Fire saved action

            Action::fire("post.saved", $post);
        }

        if ($status) {
            $message = trans("posts::posts.events.activated");
        } else {
            $message = trans("posts::posts.events.deactivated");
        }

        return redirect()->back()->with("message", $message);
    }

    /**
     * Create a new post
     * @return mixed
     */
    public function create()
    {

        $post = new Post();

        if (request()->isMethod("post")) {

            $post->title = request()->get('title');
            $post->excerpt = request()->get('excerpt');
            $post->content = request()->get('content');
            $post->image_id = request()->get('image_id', 0);
            $post->media_id = request()->get('media_id', 0);
            $post->user_id = Auth::user()->id;
            $post->status = request()->get("status", 0);
            $post->format = request()->get("format", "post");
            $post->lang = app()->getLocale();

            $post->published_at = request()->get('published_at');

            if (in_array($post->published_at, [NULL, ""])) {
                $post->published_at = date("Y-m-d H:i:s");
            }

            // Fire saving action

            Action::fire("post.saving", $post);

            if (!$post->validate()) {
                return redirect()->back()->withErrors($post->errors())->withInput(request()->all());
            }

            $post->save();
            $post->syncTags(request()->get("tags", []));
            $post->categories()->sync(request()->get("categories", []));

            // Saving post meta

            $custom_fields = array_filter(array_combine(request()->get("custom_names", []), request()->get("custom_values", [])));

            foreach ($custom_fields as $name => $value) {
                $meta = new PostMeta();
                $meta->name = $name;
                $meta->value = $value;
                $post->meta()->save($meta);
            }

            // Fire saved action

            Action::fire("post.saved", $post);

            return redirect()->route("admin.posts.edit", array("id" => $post->id))
                ->with("message", trans("posts::posts.events.created"));
        }

        $this->data["post_tags"] = array();
        $this->data["post_categories"] = collect([]);
        $this->data["post"] = $post;

        return view()->make("posts::edit", $this->data);
    }

    /**
     * Edit post by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $post = Post::findOrFail($id);

        if (request()->isMethod("post")) {

            $post->title = request()->get('title');
            $post->excerpt = request()->get('excerpt');
            $post->content = request()->get('content');
            $post->image_id = request()->get('image_id', 0);
            $post->media_id = request()->get('media_id', 0);
            $post->status = request()->get("status", 0);
            $post->format = request()->get("format", "post");
            $post->published_at = request()->get('published_at') != "" ? request()->get('published_at') : date("Y-m-d H:i:s");
            $post->lang = app()->getLocale();

            // Fire saving action

            Action::fire("post.saving", $post);

            if (!$post->validate()) {
                return redirect()->back()->withErrors($post->errors())->withInput(request()->all());
            }

            $post->save();
            $post->categories()->sync(request()->get("categories", []));
            $post->syncTags(request()->get("tags", []));

            // Fire saved action

            PostMeta::where("post_id", $post->id)->delete();

            $custom_fields = array_filter(array_combine(request()->get("custom_names", []), request()->get("custom_values", [])));

            foreach ($custom_fields as $name => $value) {
                $meta = new PostMeta();
                $meta->name = $name;
                $meta->value = $value;
                $post->meta()->save($meta);
            }

            // Fire saved action

            Action::fire("post.saved", $post);

            return redirect()->route("admin.posts.edit", array("id" => $id))->with("message", trans("posts::posts.events.updated"));
        }

        $this->data["post_tags"] = $post->tags->pluck("name")->toArray();
        $this->data["post_categories"] = $post->categories;
        $this->data["post"] = $post;

        return view()->make("posts::edit", $this->data);
    }
}
