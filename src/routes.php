<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:posts.manage"],
    "namespace" => "Dot\\Posts\\Controllers"
], function ($route) {
    $route->group(["prefix" => "posts"], function ($route) {
        $route->any('/', ["as" => "admin.posts.show", "uses" => "PostsController@index"]);
        $route->any('/create', ["as" => "admin.posts.create", "uses" => "PostsController@create"]);
        $route->any('/{id}/edit', ["as" => "admin.posts.edit", "uses" => "PostsController@edit"]);
        $route->any('/delete', ["as" => "admin.posts.delete", "uses" => "PostsController@delete"]);
        $route->any('/{status}/status', ["as" => "admin.posts.status", "uses" => "PostsController@status"]);
        $route->post('newSlug', 'PostsController@new_slug');
    });
});
